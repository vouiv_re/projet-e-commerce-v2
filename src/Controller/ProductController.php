<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Form\ProductType;
use App\Entity\Product;


class ProductController extends Controller
{
    /**
     * @Route("/product", name="product")
     */
    public function index(Request $request)
    {
        $product = new Product();
        

        $form = $this->createForm(ProductType::class,$product);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            
            $em->persist($form->getData());

            $em->flush();

        }

        return $this->render('product/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
